package com.company.newnew;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class NewnewApplication {

    public static void main(String[] args) {
        SpringApplication.run(NewnewApplication.class, args);
    }
}
