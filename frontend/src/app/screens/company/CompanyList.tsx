import { gql } from "@amplicode/gql";
import { ResultOf } from "@graphql-typed-document-node/core";
import { Datagrid, DeleteButton, EditButton, List, TextField } from "react-admin";

const COMPANY_LIST = gql(`query CompanyList {
  companyList {
    id
    name
  }
}`);

const DELETE_COMPANY = gql(`mutation DeleteCompany($id: ID!) {
  deleteCompany(id: $id) 
}`);

export const CompanyList = () => {
  const queryOptions = {
    meta: {
      query: COMPANY_LIST,
      resultDataPath: "",
    },
  };

  return (
    <List<ItemType> queryOptions={queryOptions} exporter={false} pagination={false}>
      <Datagrid rowClick="show" bulkActionButtons={false}>
        <TextField source="id" sortable={false} />

        <TextField source="name" sortable={false} />

        <EditButton />
        <DeleteButton
          mutationMode="pessimistic"
          mutationOptions={{ meta: { mutation: DELETE_COMPANY } }}
        />
      </Datagrid>
    </List>
  );
};

/**
 * Type of data object received when executing the query
 */
type QueryResultType = ResultOf<typeof COMPANY_LIST>;
/**
 * Type of the items list
 */
type ItemListType = QueryResultType["companyList"];
/**
 * Type of single item
 */
type ItemType = { id: string } & Exclude<Exclude<ItemListType, null | undefined>[0], undefined>;
