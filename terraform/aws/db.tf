module "newnew_db" {
  source = "./db"

  name                                  = var.newnew_db_name
  engine                                = var.newnew_db_engine
  engine_version                        = var.newnew_db_engine_version
  instance_class                        = var.newnew_db_instance_class
  storage                               = var.newnew_db_storage
  user                                  = var.newnew_db_user
  password                              = var.newnew_db_password
  random_password                       = var.newnew_db_random_password
  vpc_id                                = module.vpc.id
  subnet_group_name                     = module.vpc.db_subnet_group_name
  multi_az                              = var.newnew_db_multi_az
  source_security_group_id              = module.beanstalk.aws_security_group.id
}
